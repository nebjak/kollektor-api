import TagDAO from './../dao/TagDAO';
import { MongoClient } from 'mongodb';
import TestUtil from './testUtil';

let client: MongoClient;

beforeAll(async () => {
  client = await TestUtil.initDB(TagDAO);
});

afterAll(async () => {
  await client.close();
});

afterEach(async () => {
  await TestUtil.clearCollection(TagDAO.collectionName);
});

describe('TagDAO', () => {
  test('#addTag creates new tag', async () => {
    const result = await TagDAO.addTag({ name: 'Test 1' });
    expect(result).toHaveProperty('_id');
    expect(result).toHaveProperty('created_at');
    expect(result).toHaveProperty('updated_at');
  });

  test('#getTags returns array of all tags', async () => {
    const tags = new Array();
    for (let i = 0; i < 5; i++) {
      tags.push(await TagDAO.addTag({ name: `Test #${i}` }));
    }

    const result = await TagDAO.getTags();
    expect(result).toBeInstanceOf(Array);
    expect(result).toHaveLength(tags.length);
    expect(result[3].name).toBe('Test #3');
  });

  test('#getTagById finds one Tag by id', async () => {
    const tags = new Array();
    for (let i = 0; i < 5; i++) {
      tags.push(await TagDAO.addTag({ name: `Test #${i}` }));
    }

    const result = await TagDAO.getTagById(tags[2]._id);

    expect(result._id).toEqual(tags[2]._id);
    expect(result.name).toBe(tags[2].name);
  });

  test('#deleteTag', async () => {
    const tags = new Array();
    for (let i = 0; i < 5; i++) {
      tags.push(await TagDAO.addTag({ name: `Test #${i}` }));
    }

    const deleteTagResult = await TagDAO.deleteTag(tags[0]._id);
    const getTagResult = await TagDAO.getTags();
    expect(deleteTagResult).toBe(1);
    expect(getTagResult).toHaveLength(tags.length - 1);
  });

  test('#updateTag', async () => {
    const tagBefore = await TagDAO.addTag({ name: 'Test' });
    const updateResult = await TagDAO.updateTag(tagBefore._id, {
      name: 'Test after update'
    });
    const tagAfter = await TagDAO.getTagById(tagBefore._id);
    expect(updateResult).toBe(1);
    expect(tagAfter.name).toBe('Test after update');
  });
});
