import './../lib/env';
import { MongoClient } from 'mongodb';

let client: MongoClient;

export default class TestUtil {
  public static async initDB(dao: any) {
    client = await MongoClient.connect(`${process.env.DB_URI}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    dao.innjectDB(client);
    return client;
  }

  public static async clearCollection(collectionName: string) {
    await client
      .db(`${process.env.DB_NAME}_${process.env.NODE_ENV}`)
      .collection(collectionName)
      .deleteMany({});
  }
}
