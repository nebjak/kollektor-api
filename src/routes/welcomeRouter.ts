import { Router } from 'express';
import WelcomeController from './../controllers/welcomeController';

const router = Router();

router.get('/', WelcomeController.index);

export default router;
