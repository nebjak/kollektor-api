import { Router } from 'express';
import DotController from './../controllers/dotController';

const router = Router();

router.get('/:id', DotController.show);

export default router;
