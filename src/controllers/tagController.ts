import { Request, Response } from 'express';

export default class TagController {
  /**
   * Handles GET request to get list of tags
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  static index(req: Request, res: Response) {}

  /**
   * Handles GET request to show single Tag
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  static show(req: Request, res: Response) {}

  /**
   * Handles POST request to create new Tag
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  static create(req: Request, res: Response) {}

  /**
   * Handles PATCH request to update Tag
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  static update(req: Request, res: Response) {}

  /**
   * Handles DELETE request to delete Tag
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  static destroy(req: Request, res: Response) {}
}
