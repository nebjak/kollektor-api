import path from 'path';
import { Request, Response } from 'express';
import Bowser from 'bowser';
import TagLogDAO from './../dao/TagLogDAO';

export default class DotController {
  private static dotFilePath: string = path.join(
    __dirname,
    './../../static/dot.png'
  );

  static show(req: Request, res: Response) {
    const userAgent = req.headers['user-agent'] || '';
    const reqData = {
      tagId: req.params.id,
      userAgent: Bowser.parse(userAgent),
      ip: req.ip
    };

    TagLogDAO.addTagLog(reqData)
      .then(data => {
        console.dir(data);
      })
      .catch(err => {
        console.error(err);
      });

    res.sendFile(DotController.dotFilePath);
  }
}
