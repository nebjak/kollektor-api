import { Request, Response } from 'express';

export default class WelcomeController {
  static index(req: Request, res: Response) {
    res.send('<code>Hello from Kollektor</code>');
  }
}
