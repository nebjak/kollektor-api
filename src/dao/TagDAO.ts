import { MongoClient, Collection, ObjectId } from 'mongodb';

let tags: Collection;

export interface TagData {
  name?: string;
  created_at?: Date;
  updated_at?: Date;
}

export default class TagDAO {
  public static collectionName = 'tags';

  public static async innjectDB(client: MongoClient) {
    if (tags) return;
    try {
      tags = await client
        .db(`${process.env.DB_NAME}_${process.env.NODE_ENV}`)
        .collection(this.collectionName);
    } catch (err) {
      console.error(`Unable to establish collection handles in TagDAO: ${err}`);
    }
  }

  public static async addTag(data: TagData) {
    const timestamp = new Date();
    const tagDoc = {
      ...data,
      created_at: timestamp,
      updated_at: timestamp
    };

    try {
      const result = await tags.insertOne(tagDoc);
      return result.ops[0];
    } catch (err) {
      console.error('Unable to create document\n' + err);
      return [];
    }
  }

  /**
   * Returns array of all Tags
   */
  public static async getTags() {
    try {
      const result = await tags.find();
      return result.toArray();
    } catch (err) {
      return [];
    }
  }

  /**
   * Finds one Tag by _id
   * @param {string} id - string representation of Tag _id
   */
  public static async getTagById(id: string) {
    try {
      const result = await tags.findOne({ _id: new ObjectId(id) });
      return result;
    } catch (err) {
      console.error(`Unable to tag by id: ${err}`);
      return null;
    }
  }

  /**
   * Deletes Tag by _id
   * @param {string} id - string representation of Tag _id
   */
  public static async deleteTag(id: string) {
    try {
      const result = await tags.deleteOne({ _id: new ObjectId(id) });
      return result.deletedCount;
    } catch (err) {
      console.error(`Unable to delete tag: ${err}`);
      return { error: err };
    }
  }

  /**
   * Updates Tag
   * @param {string} id - string representation of Tag _id
   * @param {Object} tagData - update data in form of object
   */
  public static async updateTag(id: string, tagData: Object) {
    try {
      const result = await tags.updateOne(
        { _id: new ObjectId(id) },
        {
          $set: {
            ...tagData
          }
        }
      );

      return result.matchedCount;
    } catch (err) {
      console.error(`Unable to update tag: ${err}`);
      return { error: err };
    }
  }

  // TODO: validation
}
