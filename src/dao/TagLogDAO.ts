import { MongoClient, Collection } from 'mongodb';

let taglogs: Collection;

export default class TagLogDAO {
  public static async innjectDB(client: MongoClient) {
    if (taglogs) return;
    try {
      taglogs = await client
        .db(`${process.env.DB_NAME}_${process.env.NODE_ENV}`)
        .collection('taglogs');
    } catch (err) {
      console.error(
        `Unable to establish collection handles in TagLogDAO: ${err}`
      );
    }
  }

  public static async addTagLog(taglogData: Object) {
    try {
      const taglogDoc = {
        ...taglogData,
        created_at: new Date()
      };
      const result = await taglogs.insertOne(taglogDoc);
      return result;
    } catch (err) {
      return { err };
    }
  }
}
