import app from './app';
import http from 'http';

// TODO: implement https server

const httpPort = process.env.HTTP_PORT || 3000;
// const httpsPort = process.env.HTTPS_PORT || 3443;

const httpServer = http.createServer(app);

httpServer.listen(httpPort);
