import express from 'express';
import { MongoClient } from 'mongodb';
import WelcomeRouter from './routes/welcomeRouter';
import DotRouter from './routes/dotRouter';

import './lib/env';

import TagLogDAO from './dao/TagLogDAO';
import TagDAO from './dao/TagDAO';

const app = express();

// connect to DB
MongoClient.connect(`${process.env.DB_URI}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .catch(err => {
    console.log(err.stack);
    process.exit(1);
  })
  .then(async client => {
    await TagLogDAO.innjectDB(client);
    await TagDAO.innjectDB(client);
    console.log('Database connected 👨‍💻');
  });

app.use('/', WelcomeRouter);
app.use('/api/v1/dot', DotRouter);

export default app;
